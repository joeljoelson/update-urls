﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Server;
using Microsoft.Office.Server.Administration;
using Microsoft.Office.Server.UserProfiles;
using Microsoft.SharePoint;
using System.Web;

namespace membergroupUrl
{
    using System.Linq.Expressions;

    using Microsoft.Office.Server.Auditing;
    using Microsoft.SharePoint.Portal.WebControls;

    internal class Program
    {
        private static string siteUrl = "https://intranet.ssab.com";

        private static string urlStartsWith = "http://mysite.";

        private static void Main(string[] args)
        {
            //changeUrl is for testing purposes only.
            //changeUrl("http://dev/communities/Community%20Site");
            
            Console.WriteLine("Make a choice:");
            Console.WriteLine("Type t or test to test a specific site");
            Console.WriteLine("s to run script");
            Console.WriteLine("any other key to exit");
            var choice = Console.ReadLine();
            Console.WriteLine(choice + " " + choice.GetType());
            
            if (choice.Equals("t"))
            {
                Console.WriteLine("enter test site url");
                string url = Console.ReadLine();
                Console.WriteLine("proceed with url?(y/n)  " + url);
                var secondChoice = Console.ReadLine();
                if (secondChoice.Equals("y"))
                {
                    UpdateUrl(url);
                    Console.WriteLine("done");
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("ok bye");
                }

            }
            else if (choice.Equals("s"))
            {

                // save all relevant urls to a list
                List<string> memberGroups = GetMemberGroups();
                
                //print all affected membergroups before running
                foreach (string url in memberGroups)
                {
                    Console.WriteLine(url);
                }
                Console.WriteLine("confirm affected site list?(y/n)");
                var secondChoice = Console.ReadLine();
                
                if (secondChoice.Equals("y"))
                {
                    foreach (string url in memberGroups)
                    {
                        if (secondChoice.Equals("c"))
                        {
                            UpdateUrl(url);
                        }
                        else if (secondChoice.Equals("y"))
                        {
                            Console.WriteLine(url + "\n proceed with url? (y/n)");
                            var thirdChoice = Console.ReadLine();
                            if (thirdChoice.Equals("y"))
                            {
                                UpdateUrl(url);
                            }
                        }

                    }
                    Console.WriteLine("done");
                    Console.ReadKey();
                }

                Console.WriteLine("done");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("bye");
                Console.ReadKey();
            }
        }

        

        // this method is for testing purposes only
        private static void changeUrl(string webUrl)
        {
            using (SPSite site = new SPSite(webUrl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPServiceContext context = SPServiceContext.GetContext(site);
                    UserProfileManager profileManager = new UserProfileManager(context);
                    var memberGroupManager = profileManager.GetMemberGroups();

                    string webID = web.ID.ToString();
                    MemberGroup memberGroup =
                        profileManager.GetMemberGroups()
                            .GetMemberGroupBySourceAndSourceReference(
                                PrivacyPolicyIdConstants.MembershipsFromSharePointSites,
                                webID);
                    Console.WriteLine("old url:" + memberGroup.Url);
                    memberGroup.Url = "http://mysite.dev.com/communities/Community%20Site";
                   
                    Console.WriteLine("new url:" + memberGroup.Url + "\n");
                    memberGroup.Commit();
                    Console.ReadLine();

                    // We need to update the ETAG and last modfied property so the user's browser knows there has occured na update and refresh the browser cache.
                    foreach (Membership membership in memberGroup.GetAllMemberships())
                    {
                        UpdateModifiedUserProfileProperty(membership.ItemOwner);
                    }

                }

            }
        }

        private static List<string> GetMemberGroups()
        {
            List<string> urls = new List<string>();
            try
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPServiceContext context = SPServiceContext.GetContext(site);
                        UserProfileManager profileManager = new UserProfileManager(context);
                        var memberGroupManager = profileManager.GetMemberGroups();

                        foreach (MemberGroup group in memberGroupManager)
                        {

                            if (group.Url.StartsWith(urlStartsWith)
                                && (group.Url.Contains("/teams/") || group.Url.Contains("/projects/")
                                    || group.Url.Contains("/organizations/") || group.Url.Contains("/communities/")))
                            {
                                Console.WriteLine("found url needing an update: " + group.Url);
                                string realUrl = group.Url.Replace("http://mysite.", "http://intranet.");
                                                               
                                Console.WriteLine("actual URL: " + realUrl);
                                urls.Add(realUrl);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Couldn't get membergroups for site " + siteUrl);
                
            }
            
            return urls;
        }

        private static void UpdateUrl(string url)
        {
            
            try
            {
                using (SPSite site = new SPSite(url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPServiceContext context = SPServiceContext.GetContext(site);
                        UserProfileManager profileManager = new UserProfileManager(context);
                        var memberGroupManager = profileManager.GetMemberGroups();

                        Guid webID = web.ID;
                        MemberGroup memberGroup =
                            profileManager.GetMemberGroups()
                                .GetMemberGroupBySourceAndSourceReference(
                                    PrivacyPolicyIdConstants.MembershipsFromSharePointSites,
                                    webID.ToString());
                        Console.WriteLine("old url:" + memberGroup.Url);
                        memberGroup.Url = url;

                        Console.WriteLine("new url" + memberGroup.Url + "\n");
                        memberGroup.Commit();
                        Console.ReadLine();

                        // We need to update the ETAG and last modfied property so the user's browser knows there has occured na update and refresh the browser cache.
                        foreach (Membership membership in memberGroup.GetAllMemberships())
                        {
                            UpdateModifiedUserProfileProperty(membership.ItemOwner);
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error trying to update url: " + url);
                Console.WriteLine(ex);
            }
        }

        public static void UpdateModifiedUserProfileProperty(UserProfile currentUserProfile)
        {
            currentUserProfile["MyCollabSitesEtagLastModified"].Value = Guid.NewGuid() + "," + DateTime.UtcNow.ToString("s");
            currentUserProfile.Commit();
        }
    }
}
